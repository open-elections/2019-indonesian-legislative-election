# 2019 Indonesian legislative election

    start_date: 2019-04-17
    end_date: 2019-04-17
    source: https://pemilu2019.kpu.go.id/#/dprri/rekapitulasi/
    wikipedia: https://en.wikipedia.org/wiki/2019_Indonesian_general_election#Legislative

**The candidates file does not specify which candidates should be active. If you want any candidates to show up on Open Elections, add a column with the header `active` and add `TRUE` to any candidate you want to include.**